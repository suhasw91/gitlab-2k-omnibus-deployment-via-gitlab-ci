stages:
  - preflight
  - download
  - pre-deploy
  - deploy
  - health-check
  - pre-install-setup
  - pre-install
  - pause
  - update-gitlab
  - register-runners
  - destroy

variables:
  GL_VERSION: '15.5.2'
  RUNNER_VERSION: '15.5.1'
  SITE_DOMAIN: 'git-demo'
  GITLAB_DOMAIN: '${CI_COMMIT_BRANCH}.gitlab2'
  REGISTRY_DOMAIN: '${CI_COMMIT_BRANCH}.registry'
  PAGES_DOMAIN: '${CI_COMMIT_BRANCH}.pages'
  K8S_REDEPLOY:
    description: "Enter 'yes' to redeploy K8s runner."
    value: "no"
  GITLAB_REDEPLOY:
    description: "Enter 'yes' to redeploy GitLab."
    value: "no"

before_script:
  - set -eup
  - echo ${CI_COMMIT_BRANCH}
  - source vms/default
  - source vms/${HOSTNAME/-${CI_COMMIT_BRANCH}}
  - IP_ADDRESS="$(hostname -I | cut -f 1 -d ' ')"
  - df -h

shellcheck:
  stage: preflight
  script:
    - docker pull koalaman/shellcheck:stable
    - docker run --rm -v "$PWD:/mnt" koalaman/shellcheck:stable -x scripts/*
  tags:
    - gitlab-server


is-gitlab-on:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $GITLAB_REDEPLOY == "yes"
      changes:
        - vms/*
        - configs/gitlab/*
  stage: preflight
  script:
    - timeout 20 curl -s https://${GITLAB_DOMAIN}.${SITE_DOMAIN}.${ROOT_DOMAIN}
    - >
      if [[ $(grep -r 'SOFTWARE=gitlab' vms) ]]; then
        cp ci_templates/teardown-ci.template teardown-ci.yml
        for VM in vms/*; do 
          if [[ $(grep DO_NAME "${VM}") ]]; then 
            source ${VM}
            if [[ -v SOFTWARE ]] && [[ "${SOFTWARE}" == 'gitlab' ]]; then 
              sed "s/DO_NAME/${DO_NAME}/" ci_templates/teardown-ci.partial | tee -a teardown-ci.yml
            fi
          fi
        done
      fi
  tags:
    - gitlab-server
  allow_failure: true
  artifacts:
    paths:
      - teardown-ci.yml

teardown:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $GITLAB_REDEPLOY == "yes"
      changes:
        - vms/*
        - configs/gitlab/*
  stage: download
  allow_failure: true
  trigger:
    include:
      - artifact: teardown-ci.yml
        job: is-gitlab-on
    strategy: depend

delete_vars:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $GITLAB_REDEPLOY == "yes"
      changes:
        - vms/*
        - configs/gitlab/*
  stage: download
  script:
    - >
      for SECRET in $(env | grep "CI_SECRET" | grep "${CI_COMMIT_BRANCH}"  | grep -o -P '.*(?==)' ); do 
        echo "Deleting ${SECRET}"
        curl -s --request DELETE --header "PRIVATE-TOKEN:${GL_TOKEN}" "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/variables/${SECRET}"
        sleep 10
      done
  tags:
    - gitlab-server
  dependencies: []


download-gitlab:
  stage: download
  script:
    - PRESENT_PACKAGE=$(curl -s --header "JOB-TOKEN:$CI_JOB_TOKEN" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages" | jq ".[] | select(.name==\"gitlab-deb\" and .version==\"${GL_VERSION}\") | .version" | tr -d \")
    - >
      if [[ "${PRESENT_PACKAGE}" = "${GL_VERSION}" ]]; then 
        exit 0
      fi
    - wget -q --content-disposition https://packages.gitlab.com/gitlab/gitlab-ee/packages/ubuntu/bionic/gitlab-ee_${GL_VERSION}-ee.0_amd64.deb/download.deb
    - curl --header "JOB-TOKEN:$CI_JOB_TOKEN" --upload-file gitlab-ee_${GL_VERSION}-ee.0_amd64.deb "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/gitlab-deb/${GL_VERSION}/gitlab-ee_${GL_VERSION}-ee.0_amd64.deb"
  tags:
    - gitlab-server
  dependencies: []


download-runner:
  stage: download
  script:
    - PRESENT_PACKAGE=$(curl -s --header "JOB-TOKEN:$CI_JOB_TOKEN" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages" | jq ".[] | select(.name==\"runner-deb\" and .version==\"${RUNNER_VERSION}\")")
    - >
      if [[ "${PRESENT_PACKAGE}" ]]; then 
        exit 0
      fi
    - curl -o gitlab-runner_${RUNNER_VERSION}_amd64.deb -LJ https://gitlab-runner-downloads.s3.amazonaws.com/v${RUNNER_VERSION}/deb/gitlab-runner_amd64.deb
    - curl --header "JOB-TOKEN:$CI_JOB_TOKEN" --upload-file gitlab-runner_${RUNNER_VERSION}_amd64.deb "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/runner-deb/${RUNNER_VERSION}/gitlab-runner_${RUNNER_VERSION}_amd64.deb"
  tags:
    - gitlab-server
  dependencies: []


remove_runners:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $GITLAB_REDEPLOY == "yes"
      changes:
        - vms/*
        - configs/gitlab/*
  stage: pre-deploy
  script:
    - OLD_RUNNERS=( $(curl -s --header "PRIVATE-TOKEN:$GL_TOKEN" "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/runners?tag_list=gitlab-${CI_COMMIT_BRANCH}-demo&per_page=100" | jq '.[].id') )
    - >
      for OLD_RUNNER in ${OLD_RUNNERS[*]}; do 
        curl -s --request DELETE --header "PRIVATE-TOKEN:$GL_TOKEN" "https://${CI_SERVER_HOST}/api/v4/runners/$OLD_RUNNER"
      done
  tags:
    - gitlab-server
  allow_failure: true
  dependencies: []

make-loadbalancers:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $GITLAB_REDEPLOY == "yes"
      changes:
        - vms/*
        - configs/gitlab/*
  stage: pre-deploy
  script:
    - >
      for VM in vms/*; do 
        unset LB
        source "${VM}"
        if [[ -v LB ]]; then 
          bash scripts/make-loadbalancer.sh "${VM}"
        fi
      done
  tags:
    - gitlab-server

make-ca:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $GITLAB_REDEPLOY == "yes"
      changes:
        - vms/*
        - configs/gitlab/*
  stage: pre-deploy
  script:
    - openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -subj '/CN=Technolibre Root CA/C=US/ST=Tennessee/L=Knoxville/O=Technolibre' -keyout ca.key -out ca.crt
    - curl -s --request POST --header "PRIVATE-TOKEN:${GL_TOKEN}" "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/variables" --form "key=CI_SECRET_CA_KEY_${CI_COMMIT_BRANCH}" --form "value=$(cat ca.key)" --form "variable_type=file" >/dev/null
    - curl -s --request POST --header "PRIVATE-TOKEN:${GL_TOKEN}" "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/variables" --form "key=CI_SECRET_CA_CRT_${CI_COMMIT_BRANCH}" --form "value=$(cat ca.crt)" --form "variable_type=file" >/dev/null
  tags:
    - gitlab-server
  dependencies: []

rd-secrets-prep:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $GITLAB_REDEPLOY == "yes"
      changes:
        - vms/*
        - configs/gitlab/*
  stage: pre-deploy
  script:
    - DB_PASSWORD="$(echo $RANDOM | md5sum | cut -f 1 -d ' ')"
    - DB_PASSWORD_HASH=$(printf "%s\n" "${DB_PASSWORD}" | gitlab-ctl pg-password-md5 gitlab)
    - curl -s --request POST --header "PRIVATE-TOKEN:${GL_TOKEN}" "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/variables" --form "key=CI_SECRET_GITLAB_RB_DB_PASSWORD_${CI_COMMIT_BRANCH}" --form "value=${DB_PASSWORD}" --form "masked=yes" >/dev/null
    - curl -s --request POST --header "PRIVATE-TOKEN:${GL_TOKEN}" "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/variables" --form "key=CI_SECRET_GITLAB_RB_DB_HASH_${CI_COMMIT_BRANCH}" --form "value=${DB_PASSWORD_HASH}" --form "masked=yes" >/dev/null
    - curl -s --request POST --header "PRIVATE-TOKEN:${GL_TOKEN}" "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/variables" --form "key=CI_SECRET_GITLAB_RB_GITALY_AUTH_TOKEN_${CI_COMMIT_BRANCH}" --form "value=$(echo $RANDOM | md5sum | cut -f 1 -d ' ')" --form "masked=yes" >/dev/null
    - curl -s --request POST --header "PRIVATE-TOKEN:${GL_TOKEN}" "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/variables" --form "key=CI_SECRET_GITLAB_RB_REDIS_PASSWORD_${CI_COMMIT_BRANCH}" --form "value=$(echo $RANDOM | md5sum | cut -f 1 -d ' ')" --form "masked=yes" >/dev/null
    - curl -s --request POST --header "PRIVATE-TOKEN:${GL_TOKEN}" "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/variables" --form "key=CI_SECRET_GITLAB_RB_ROOT_PASSWORD_${CI_COMMIT_BRANCH}" --form "value=$(echo $RANDOM | md5sum | cut -f 1 -d ' ')" --form "masked=yes" >/dev/null
    - curl -s --request POST --header "PRIVATE-TOKEN:${GL_TOKEN}" "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/variables" --form "key=CI_SECRET_GITLAB_RB_REGISTRATION_TOKEN_${CI_COMMIT_BRANCH}" --form "value=$(echo $RANDOM | md5sum | cut -f 1 -d ' ')" --form "masked=yes" >/dev/null
  tags:
    - gitlab-server
  dependencies: []

block-storage:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $GITLAB_REDEPLOY == "yes"
      changes:
        - vms/*
        - configs/gitlab/*
  stage: pre-deploy
  script:
    - >
      for VM in vms/*; do 
        bash scripts/create-storage.sh "${VM}"
      done
  tags:
    - gitlab-server
  dependencies: []

delete-vms:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $GITLAB_REDEPLOY == "yes"
      changes:
        - vms/*
        - configs/gitlab/*
  stage: pre-deploy
  script:
    - >
      for VM in vms/*; do 
        bash scripts/delete-vm.sh ${VM}
      done
    - curl -X DELETE -H "Content-Type:\ application/json" -H "Authorization:\ Bearer $DO_TOKEN" "https://api.digitalocean.com/v2/droplets?tag_name=gitlab-${CI_COMMIT_BRANCH}-demo"
    - sleep 20
  tags:
    - gitlab-server
  dependencies: []

delete-k8s:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $K8S_REDEPLOY == "yes"
      changes:
        - k8s/*
        - configs/k8s/*
        - ci_templates/k8s*
  stage: pre-deploy
  script:
    - >
      K8S=$(curl -s -X GET \
                    -H "Content-Type:\ application/json" \
                    -H "Authorization:\ Bearer ${DO_TOKEN}" \
                    "https://api.digitalocean.com/v2/kubernetes/clusters" | \
            jq ".kubernetes_clusters[] | select (.tags[]==\"${CI_COMMIT_BRANCH}\") | .id" | \
            tr -d \")
      curl -s -X DELETE \
           -H "Content-Type:\ application/json" \
           -H "Authorization:\ Bearer ${DO_TOKEN}" \
           "https://api.digitalocean.com/v2/kubernetes/clusters/${K8S}"
  tags:
    - gitlab-server
  dependencies: []


deploy-vms:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $GITLAB_REDEPLOY == "yes"
      changes:
        - vms/*
        - configs/gitlab/*
  stage: deploy
  script:
    - >
      for VM in vms/*demo*; do 
        bash scripts/create-vm.sh "${VM}"
      done
    - >
      if [[ $(grep -r DO_NAME vms) ]]; then
        cp ci_templates/health-check-ci.template health-check-ci.yml
        for VM in vms/*; do 
          if [[ $(grep DO_NAME "${VM}") ]]; then 
            source ${VM}
            sed "s/DO_NAME/${DO_NAME}/" ci_templates/health-check-ci.partial | tee -a health-check-ci.yml
          fi
        done
      else
        cp ci_templates/health-check-ci.blank health-check-ci.yml
      fi
  tags:
    - gitlab-server
  artifacts:
    paths:
      - health-check-ci.yml
  dependencies: []

deploy-k8s:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $K8S_REDEPLOY == "yes"
      changes:
        - k8s/*
        - configs/k8s/*
        - ci_templates/k8s*
  stage: deploy
  script:
    - >
      for K8S in k8s/*demo*; do 
        bash scripts/create-k8s.sh "${K8S}"
      done
    - >
      if [[ $(grep -r K8S_NAME k8s) ]]; then
        cp ci_templates/k8s-runner-register-ci.template k8s-runner-register-ci.yml
        for K8S in k8s/*; do 
          if [[ $(grep K8S_NAME "${K8S}") ]]; then 
            source ${K8S}
            sed "s/K8S/${K8S#*/}/" ci_templates/k8s-runner-register-ci.partial | tee -a k8s-runner-register-ci.yml
          fi
        done
      else
        cp ci_templates/k8s-runner-register-ci.blank k8s-runner-register-ci.yml
      fi
  tags:
    - gitlab-server
  artifacts:
    paths:
      - k8s-runner-register-ci.yml
  dependencies: []

health-check:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $GITLAB_REDEPLOY == "yes"
      changes:
        - vms/*
        - configs/gitlab/*
  stage: health-check
  trigger:
    include:
      - artifact: health-check-ci.yml
        job: deploy-vms
    strategy: depend

pre-install-setup:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $GITLAB_REDEPLOY == "yes"
      changes:
        - vms/*
        - configs/gitlab/*
  stage: pre-install-setup
  script:
    - >
      if [[ $(grep -r DO_VOLUME vms) ]]; then
        echo "Copying template for attaching disks"
        cp ci_templates/attach-disk-ci.template attach-disk-ci.yml
        for VM in vms/*; do 
          echo "Checking if DO_NAME and DO_VOLUME exist in the VM definition"
          if [[ $(grep DO_NAME "${VM}") ]] && [[ $(grep DO_VOLUME "${VM}") ]]; then
            echo "Adding $VM to attach-disk-ci.yml"
            source ${VM}; sed "s/DO_NAME/${DO_NAME}/" ci_templates/attach-disk-ci.partial | tee -a attach-disk-ci.yml
          fi
        done
      else
        echo "No VMs require attaching disks."
        cp ci_templates/attach-disk-ci.blank attach-disk-ci.yml
      fi
    - >
      if [[ $(grep -r 'SOFTWARE=gitlab' vms) ]]; then
        echo "Copying template for RB deploy"
        cp ci_templates/rb-deploy-ci.template rb-deploy-ci.yml
        for VM in vms/*; do 
          echo "Checking if DO_NAME exists in the VM definition"
          if [[ $(grep DO_NAME "${VM}") ]]; then 
          source ${VM}
            if [[ -v SOFTWARE ]] && [[ "${SOFTWARE}" == 'gitlab' ]]; then
              echo "Adding $VM to rb-deploy-ci.yml"
              sed "s/DO_NAME/${DO_NAME}/" ci_templates/rb-deploy-ci.partial | tee -a rb-deploy-ci.yml
            fi
          fi
        done
      else
        echo "No GitLab VMs to create RB deploy jobs for."
        cp ci_templates/rb-deploy-ci.blank rb-deploy-ci.yml
      fi
  tags:
    - gitlab-server
  artifacts:
    paths:
      - attach-disk-ci.yml
      - rb-deploy-ci.yml
  dependencies: []

attach-disk:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $GITLAB_REDEPLOY == "yes"
      changes:
        - vms/*
        - configs/gitlab/*
  stage: pre-install
  trigger:
    include:
      - artifact: attach-disk-ci.yml
        job: pre-install-setup
    strategy: depend

rb-deploy:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $GITLAB_REDEPLOY == "yes"
      changes:
        - vms/*
        - configs/gitlab/*
  stage: pre-install
  trigger:
    include:
      - artifact: rb-deploy-ci.yml
        job: pre-install-setup
    strategy: depend

pause:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $GITLAB_REDEPLOY == "yes"
      changes:
        - vms/*
        - configs/gitlab/*
  stage: pause
  script:
    - sleep 90
    - >
      if [[ $(grep -r 'SOFTWARE=gitlab' vms) ]]; then
        cp ci_templates/update-gitlab-ci.stages update-gitlab-ci.yml
        for VM in vms/*demo*; do 
          source ${VM}
          if [[ -v SOFTWARE ]] && [[ "${SOFTWARE}" == 'gitlab' ]] && [[ -v DO_NAME ]] && [[ "${DO_NAME}" =~ 'rails' ]]; then 
            echo "  - update-gitlab-${DO_NAME}" | tee -a update-gitlab-ci.yml
          fi
        done
        echo "" | tee -a update-gitlab-ci.yml
        cat ci_templates/update-gitlab-ci.template | tee -a update-gitlab-ci.yml && echo "" | tee -a update-gitlab-ci.yml
        for VM in vms/*demo*; do 
          source ${VM}
          if [[ -v SOFTWARE ]] && [[ "${SOFTWARE}" == 'gitlab' ]] && [[ -v DO_NAME ]] && [[ ! "${DO_NAME}" =~ 'rails' ]]; then 
            sed "s/DO_NAME/${DO_NAME}/" ci_templates/update-gitlab-ci-independents.partial | tee -a update-gitlab-ci.yml; echo "" | tee -a update-gitlab-ci.yml; 
          fi
          done
        for VM in vms/*demo*; do 
          source ${VM} 
          if [[ -v SOFTWARE ]] && [[ "${SOFTWARE}" == 'gitlab' ]] && [[ -v DO_NAME ]] && [[ "${DO_NAME}" =~ 'rails' ]]; then 
            sed "s/DO_NAME/${DO_NAME}/" ci_templates/update-gitlab-ci-rails.partial | tee -a update-gitlab-ci.yml; echo "" | tee -a update-gitlab-ci.yml
          fi
        done
      else
        cp ci_templates/update-gitlab-ci.blank update-gitlab-ci.yml
      fi
  tags:
    - gitlab-server
  artifacts:
    paths:
      - update-gitlab-ci.yml
  dependencies: []

ensure-on:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $GITLAB_REDEPLOY == "yes"
      changes:
        - vms/*
        - configs/gitlab/*
  stage: update-gitlab
  script:
    - >
      for VM in vms/*; do 
        bash scripts/toggle_node_power.sh "${VM}" "power_on"
      done
  tags:
    - gitlab-server

update-gitlab:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $GITLAB_REDEPLOY == "yes"
      changes:
        - vms/*
        - configs/gitlab/*
  stage: update-gitlab
  trigger:
    include:
      - artifact: update-gitlab-ci.yml
        job: pause
    strategy: depend

register-runners:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push" || $K8S_REDEPLOY == "yes"
      changes:
        - k8s/*
        - configs/k8s/*
        - ci_templates/k8s*
  stage: register-runners
  trigger:
    include:
      - artifact: k8s-runner-register-ci.yml
        job: deploy-k8s
    strategy: depend

destroy-vms:
  stage: destroy
  when: manual
  script:
    - sudo test -e /etc/gitlab/gitlab-secrets.json && curl -s --request PUT --header "PRIVATE-TOKEN:${GL_TOKEN}" "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/variables/SECRETS_JSON_${CI_COMMIT_BRANCH}" --form "value=$(sudo cat /etc/gitlab/gitlab-secrets.json)" --form "variable_type=file" >/dev/null
    - sleep 20
    - >
      for VM in vms/*; do 
        bash scripts/delete-vm.sh ${VM}
      done
    - curl -s -X DELETE -H "Content-Type:\ application/json" -H "Authorization:\ Bearer $DO_TOKEN" "https://api.digitalocean.com/v2/droplets?tag_name=gitlab-demo"
    - OLD_RUNNERS=( $(curl -s --header "PRIVATE-TOKEN:$GL_TOKEN" "https://${CI_SERVER_HOST}/api/v4/runners/all?tag_list=gitlab-demo&per_page=100" | jq '.[].id') )
    - > 
      for OLD_RUNNER in ${OLD_RUNNERS[*]}; do 
        curl -s --request DELETE --header "PRIVATE-TOKEN:$GL_TOKEN" "https://${CI_SERVER_HOST}/api/v4/runners/$OLD_RUNNER"
      done
  tags:
    - rails-${CI_COMMIT_BRANCH}
  dependencies: []

destroy-storage:
  stage: destroy
  when: manual
  script:
    - > 
      for VM in vms/*; do 
        bash scripts/delete-storage.sh ${VM};
      done
  tags:
    - gitlab-server
  dependencies: []
