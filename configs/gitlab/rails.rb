nginx['enable'] = true
puma['enable'] = true
sidekiq['enable'] = true
gitlab_workhorse['enable'] = true

external_url "https://GITLAB_DOMAIN.SITE_DOMAIN.ROOT_DOMAIN"
nginx['redirect_http_to_https'] = true
nginx['proxy_protocol'] = true
nginx['real_ip_trusted_addresses'] = [ "127.0.0.0/16", "IP_OF_THE_PROXY/32"]


letsencrypt['enable'] = true
letsencrypt['auto_renew'] = true

gitlab_rails['initial_root_password'] = "CI_SECRET_GITLAB_RB_ROOT_PASSWORD"
gitlab_rails['initial_shared_runners_registration_token'] = "CI_SECRET_GITLAB_RB_REGISTRATION_TOKEN"

gitlab_rails['object_store']['enabled'] = true
gitlab_rails['object_store']['proxy_download'] = true
gitlab_rails['object_store']['connection'] = {
  'provider' => 'DO_SPACES_PROVIDER',
  'region' => 'DO_SPACES_REGION',
  'endpoint' => 'DO_SPACES_ENDPOINT',
  'aws_access_key_id' => 'DO_SPACES_ACCESS_TOKEN',
  'aws_secret_access_key' => 'DO_SPACES_SECRET_TOKEN'
}
gitlab_rails['object_store']['objects']['artifacts']['bucket'] = 'git-demo-technolibre-artifacts/BRANCH'
gitlab_rails['object_store']['objects']['artifacts']['proxy_download'] = true
gitlab_rails['object_store']['objects']['external_diffs']['bucket'] = 'git-demo-technolibre-diffs/BRANCH'
gitlab_rails['object_store']['objects']['lfs']['bucket'] = 'git-demo-technolibre-lfs/BRANCH'
gitlab_rails['object_store']['objects']['uploads']['bucket'] = 'git-demo-technolibre-uploads/BRANCH'
gitlab_rails['object_store']['objects']['packages']['bucket'] = 'git-demo-technolibre-packages/BRANCH'
gitlab_rails['object_store']['objects']['dependency_proxy']['enabled'] = true
gitlab_rails['object_store']['objects']['dependency_proxy']['bucket'] = 'git-demo-technolibre-proxy/BRANCH'
gitlab_rails['object_store']['objects']['terraform_state']['bucket'] = 'git-demo-technolibre-terraform/BRANCH'
gitlab_rails['object_store']['objects']['pages']['bucket'] = 'git-demo-technolibre-pages/BRANCH'

gitlab_rails['backup_upload_connection'] = {
  'provider' => 'DO_SPACES_PROVIDER',
  'region' => 'DO_SPACES_REGION',
  'endpoint' => 'DO_SPACES_ENDPOINT',
  'aws_access_key_id' => 'DO_SPACES_ACCESS_TOKEN',
  'aws_secret_access_key' => 'DO_SPACES_SECRET_TOKEN'
}
gitlab_rails['backup_upload_remote_directory'] = 'git-demo-technolibre-backups/BRANCH'

gitlab_rails['incoming_email_enabled'] = true

gitlab_rails['db_adapter'] = 'postgresql'
gitlab_rails['db_encoding'] = 'utf8'
gitlab_rails['db_host'] = 'CI_SECRET_GITLAB_RB_POSTGRESQL_1_IP'
gitlab_rails['db_port'] = 5432
gitlab_rails['db_sslmode'] = 'require'
gitlab_rails['db_username'] = 'gitlab'
gitlab_rails['db_password'] = 'CI_SECRET_GITLAB_RB_DB_PASSWORD'

gitlab_rails['redis_host'] = 'CI_SECRET_GITLAB_RB_REDIS_1_IP'
gitlab_rails['redis_port'] = 6379
gitlab_rails['redis_ssl'] = true
gitlab_rails['redis_password'] = 'CI_SECRET_GITLAB_RB_REDIS_PASSWORD'

gitlab_shell['secret_token'] = 'CI_SECRET_GITLAB_RB_GITALY_AUTH_TOKEN'

gitlab_rails['env'] = {
    "GITLAB_LICENSE_MODE" => "test",
    "CUSTOMER_PORTAL_URL" => "https://customers.staging.gitlab.com"
}
