roles(['postgres_role'])
postgresql['enable'] = true
postgresql['listen_address'] = '0.0.0.0'
postgresql['port'] = 5432
postgresql['ssl_cert_file'] = "/etc/gitlab/ssl/HOSTNAME.ROOT_DOMAIN.crt"
postgresql['ssl_key_file'] = "/etc/gitlab/ssl/HOSTNAME.ROOT_DOMAIN.key"
postgresql['ssl_ca_file'] = "/etc/gitlab/trusted-certs/ca.crt"
postgresql['hostssl'] = true

postgresql['sql_user'] = "gitlab"
postgresql['sql_user_password'] = 'CI_SECRET_GITLAB_RB_DB_HASH'
postgresql['trust_auth_cidr_addresses'] = %w(127.0.0.1/32)
postgresql['md5_auth_cidr_addresses'] = %w(0.0.0.0/0)
postgresql['uid'] = 65433
postgresql['gid'] = 65433
postgresql['shell'] = "/bin/sh"
postgresql['home'] = "/mnt/DO_VOLUME"
postgresql['dir'] = "/mnt/DO_VOLUME"
gitlab_rails['auto_migrate'] = false
