#! /bin/bash
# shellcheck disable=SC1090

set -epu

source "vms/default"
source "${1}"

curl -s -X POST "https://api.digitalocean.com/v2/droplets" -d "{\"name\":\"$DO_NAME\",\"region\":\"$DO_REGION\",\"size\":\"$DO_SIZE\",\"image\":\"$DO_IMAGE\",\"user_data\":
\"#! /bin/bash
curl -s --header \"PRIVATE-TOKEN:${GL_TOKEN}\" -o gitlab-runner.deb \"https://git.${ROOT_DOMAIN}/api/v4/projects/112/packages/generic/runner-deb/${RUNNER_VERSION}/gitlab-runner_${RUNNER_VERSION}_amd64.deb\"
dpkg -i gitlab-runner.deb
echo 'gitlab-runner    ALL=(ALL:ALL) NOPASSWD:ALL' >> /etc/sudoers
gitlab-runner register -n -u ${CI_SERVER_URL} -r ${RUNNER_TOKEN} --executor shell --tag-list \"$DO_NAME,$ROLE-${CI_COMMIT_BRANCH},gitlab-${CI_COMMIT_BRANCH}-demo\" --name $DO_NAME\",
      \"ssh_keys\":[28469334],\"tags\":[\"$DO_NAME\",\"${ROLE}-${CI_COMMIT_BRANCH}\",\"gitlab-${CI_COMMIT_BRANCH}-demo\"]}" \
      -H "Authorization: Bearer $DO_TOKEN" \
      -H "Content-Type: application/json"

if [[ -v DO_VOLUME ]]; then
  until [[ -v DO_ID ]]; do
    DO_ID=$(curl -s -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $DO_TOKEN" "https://api.digitalocean.com/v2/droplets?name=${DO_NAME}" | jq .droplets[].id)
    sleep 5
  done
  sleep 30
  source "storage/${DO_VOLUME}"
  curl -s -X POST \
       -H "Content-Type: application/json" \
       -H "Authorization: Bearer ${DO_TOKEN}" \
       -d "{\"type\": \"attach\", \"volume_name\": \"${STORAGE_NAME}\", \"region\": \"${STORAGE_REGION}\", \"droplet_id\": \"${DO_ID}\",\"tags\":[\"${STORAGE_LABEL}\"] }" \
       "https://api.digitalocean.com/v2/volumes/actions"
fi
