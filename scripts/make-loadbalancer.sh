#! /bin/bash
# shellcheck disable=SC1090

set -peu

VM="${1}"
source "vms/default"
source "${VM}"

PRESENT_LB=$(curl -s -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $DO_TOKEN" "https://api.digitalocean.com/v2/load_balancers" | jq ".load_balancers[] | select(.name == \"gitlab-${CI_COMMIT_BRANCH}-${ROLE}-lb\") | .name" | tr -d \")

if [[ ! -v PRESENT_LB ]] || [[ ! "${PRESENT_LB}" == "gitlab-${CI_COMMIT_BRANCH}-${ROLE}-lb" ]]; then
  curl -s -X POST \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer $DO_TOKEN" \
  -d "{\"name\": \"gitlab-${CI_COMMIT_BRANCH}-${ROLE}-lb\", \"region\": \"${DO_REGION}\", \"size_unit\": 1, \"forwarding_rules\":[{\"entry_protocol\":\"http\",\"entry_port\":80,\"target_protocol\":\"http\",\"target_port\":80,\"certificate_id\":\"\",\"tls_passthrough\":false}, {\"entry_protocol\": \"https\",\"entry_port\": 443,\"target_protocol\": \"https\",\"target_port\": 443,\"tls_passthrough\": true}, {\"entry_protocol\": \"tcp\",\"entry_port\": 22,\"target_protocol\": \"tcp\",\"target_port\": 22}], \"health_check\":{\"protocol\":\"tcp\",\"port\":22,\"check_interval_seconds\":10,\"response_timeout_seconds\":5,\"healthy_threshold\":5,\"unhealthy_threshold\":3}, \"enable_proxy_protocol\": true, \"tag\": \"${ROLE}-${CI_COMMIT_BRANCH}\"}" \
  "https://api.digitalocean.com/v2/load_balancers"

  echo "Had to make loadbalancer, please register it with DNS and wait 24 hours."
  exit 1
fi
