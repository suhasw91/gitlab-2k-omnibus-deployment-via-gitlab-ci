#! /bin/bash
# shellcheck disable=SC1090

set -peu

source "k8s/default"
source "${1}"

curl -s -X POST \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer $DO_TOKEN" \
  -d "{\"name\": \"${K8S_NAME}\",\"region\": \"${K8S_REGION}\",\"version\": \"${K8S_VERSION}\",\"tags\": [\"${CI_COMMIT_BRANCH}\",\"${K8S_NAME}\"],\"node_pools\": [{\"size\": \"${K8S_SIZE}\",\"count\": ${K8S_COUNT},\"name\": \"${K8S_NAME}\",\"tags\": [\"${CI_COMMIT_BRANCH}\",\"${K8S_NAME}\"],\"labels\": {\"service\": \"${K8S_NAME}\", \"priority\": \"high\"}}]}" \
  "https://api.digitalocean.com/v2/kubernetes/clusters"
