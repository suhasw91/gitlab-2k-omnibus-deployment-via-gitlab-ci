#! /bin/bash
# shellcheck disable=SC1090

set -epu

source "vms/${1}"


if [[ -v ROLE ]] && [[ "${ROLE}" = "rails" ]]; then
  unset LE_CERT_VAR LE_KEY_VAR
  LE_CERT_VAR="LE_${CI_COMMIT_BRANCH}_CRT"
  LE_KEY_VAR="LE_${CI_COMMIT_BRANCH}_KEY"
  openssl x509 -in "${!LE_CERT_VAR}" -subject -noout | cut -f 3 -d ' '
  # shellcheck disable=SC2076
  if [[ "$(openssl x509 -in "${!LE_CERT_VAR}" -subject -noout | cut -f 3 -d ' ')" =~ "${GITLAB_DOMAIN}.${SITE_DOMAIN}.${ROOT_DOMAIN}" ]] && [[ "$(openssl x509 -in "${!LE_CERT_VAR}" -issuer -noout)" =~ "Let's Encrypt" ]]; then
    echo "Checking if LE cert and key match"
    set +e
    CRTMOD=$(openssl x509 -noout -modulus -in "${!LE_CERT_VAR}" | grep -P -o '(?<=Modulus=).*')
    echo "$CRTMOD"
    KEYMOD=$(openssl rsa -check -noout -modulus -in "${!LE_KEY_VAR}" | grep -P -o '(?<=Modulus=).*')
    echo "$KEYMOD"
    set -e
    if [[ "${CRTMOD}" = "${KEYMOD}" ]]; then
      echo "Installing LE cert"
      bash scripts/install-cert.sh -c "${!LE_CERT_VAR}" -k "${!LE_KEY_VAR}" -C "/etc/gitlab/ssl/${GITLAB_DOMAIN}.${SITE_DOMAIN}.${ROOT_DOMAIN}.crt" -K "/etc/gitlab/ssl/${GITLAB_DOMAIN}.${SITE_DOMAIN}.${ROOT_DOMAIN}.key"
    fi
  fi
else
  openssl req -new -nodes -out "${HOSTNAME}.csr" -newkey rsa:4096 -keyout "${HOSTNAME}.${ROOT_DOMAIN}.key" -subj "/CN=Technolibre Root CA/C=US/ST=Tennessee/L=Knoxville/O=${HOSTNAME}.${ROOT_DOMAIN}"
  for ((i = 0; i < ${#DNS_NAMES[@]}; ++i)); do 
    echo "DNS.$(( i + 1 )) = ${DNS_NAMES[$i]}.${SITE_DOMAIN}.${ROOT_DOMAIN}" >> san.ext
  done
  echo "IP.1 = ${2}" >> san.ext
  if [[ ! -v HOSTNAME ]]; then
    echo "HOSTNAME not set."
  elif [[ ! -v CI_SECRET_CA_CRT_${CI_COMMIT_BRANCH} ]]; then
    echo "CA_CRT not set."
  elif [[ ! -v CI_SECRET_CA_KEY_${CI_COMMIT_BRANCH} ]]; then
    echo "CA_KEY not set."
  elif [[ ! -v ROOT_DOMAIN ]]; then 
    echo "DOMAIN not set."
  else
    CERT_VAR="CI_SECRET_CA_CRT_${CI_COMMIT_BRANCH}"
    KEY_VAR="CI_SECRET_CA_KEY_${CI_COMMIT_BRANCH}"
    openssl x509 -req -in "${HOSTNAME}.csr" -CA "${!CERT_VAR}" -CAkey "${!KEY_VAR}" -CAcreateserial -out "${HOSTNAME}.${ROOT_DOMAIN}.crt" -days 730 -sha256 -extfile san.ext
  fi
  bash scripts/install-cert.sh -c "${HOSTNAME}.${ROOT_DOMAIN}.crt" -k "${HOSTNAME}.${ROOT_DOMAIN}.key" -C "/etc/gitlab/ssl/${HOSTNAME}.${ROOT_DOMAIN}.crt" -K "/etc/gitlab/ssl/${HOSTNAME}.${ROOT_DOMAIN}.key"
fi
