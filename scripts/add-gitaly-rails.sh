#! /bin/bash

set -epu

GITALY_NODES=( vms/gitaly* )
NODE_TEMPLATE="  'gitalyNODE'  => { 'gitaly_address' => 'tls://CI_SECRET_GITLAB_RB_GITALY_NODE_IP:9999', 'gitaly_token' => 'CI_SECRET_GITLAB_RB_GITALY_AUTH_TOKEN' },"

if [[ ${#GITALY_NODES[*]} -gt 0 ]]; then
  echo 'git_data_dirs({' | tee -a gitlab.rb
  echo "  'default'  => { 'gitaly_address' => 'tls://CI_SECRET_GITLAB_RB_GITALY_1_IP:9999', 'gitaly_token' => 'CI_SECRET_GITLAB_RB_GITALY_AUTH_TOKEN' }," | tee -a gitlab.rb
  for (( NODE = 2; NODE <= ${#GITALY_NODES[*]}; NODE++ )); do
    echo "${NODE_TEMPLATE}" | sed "s/NODE/${NODE}/g" | tee -a gitlab.rb
  done
  echo '})' | tee -a gitlab.rb
fi
